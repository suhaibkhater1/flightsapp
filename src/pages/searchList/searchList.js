import { Observer } from "mobx-react-lite";
import React, { useEffect } from "react";

import { useFlightsStore } from "../../store/flightsStore/flightsContext";
import SearchSection from "./sections/searchSection";
import ListingSection from "./sections/listingSection";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { useHistory } from "react-router";

const SearchList = () => {
  const flightsStore = useFlightsStore();
  const useStyles = makeStyles(() => ({
    button: {
      margin: 25,
    },
  }));
  const classes = useStyles();
  let history = useHistory();
  const [nights, setNights] = React.useState(0);

  const calculateNights = () => {
    let fromDate = new Date(flightsStore.searchParams.dateRange[0]);
    let toDate = new Date(flightsStore.searchParams.dateRange[1]);

    var nights = (toDate.getTime() - fromDate.getTime()) / (1000 * 3600 * 24);

    // the plus 1 is for the first night
    setNights(Math.ceil(nights + 1));
  };

  useEffect(() => {
    if (!flightsStore.searchParams.dateRange) {
      history.push("/");
      return;
    }
    calculateNights();
  }, []);

  return (
    <Observer>
      {() => (
        <div className="container">
          <div className="row">
            <div className="col-md-4 col-12">
              <SearchSection
                onTextChange={(e) => {
                  flightsStore.setSearchParams({ name: e.target.value });
                }}
                onPriceChange={(newValue) => {
                  flightsStore.setSearchParams({ priceRange: newValue });
                }}
              />
            </div>

            <div className="col-md-8 col-12 pt-3">
              <div className="row d-flex justify-content-between align-items-center">
                <div className="col-md-6 col-12">
                  <h3>Total Nights:{nights}</h3>
                </div>
                <div className="col-md-6 col-12 d-flex justify-content-around align-items-center">
                  <Button
                    variant="outlined"
                    color="primary"
                    className={classes.button}
                    onClick={flightsStore.sortByName}
                  >
                    Sort By Name
                  </Button>
                  <Button
                    variant="outlined"
                    color="primary"
                    className={classes.button}
                    onClick={flightsStore.sortByPrice}
                  >
                    Sort By Price
                  </Button>
                </div>
              </div>
              <ListingSection />
            </div>
          </div>
        </div>
      )}
    </Observer>
  );
};

export default SearchList;
