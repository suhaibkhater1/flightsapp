import { Observer } from "mobx-react-lite";
import React from "react";
import { useFlightsStore } from "../../../store/flightsStore/flightsContext";
import SearchField from "../../../components/searchField/searchField"
import Slider from "../../../components/slider/slider";

const SearchSection = ({onTextChange,onPriceChange}) => {
  const flightsStore = useFlightsStore();

  return (
    <Observer>
      {() => <>
         <SearchField onChange={onTextChange}/>
         <Slider onChange={onPriceChange} title="Price" min={0} max={600}/>
        </>
      }
    </Observer>
  );
};

export default SearchSection;
