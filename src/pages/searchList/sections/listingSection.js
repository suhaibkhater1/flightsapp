import { Observer } from "mobx-react-lite";
import React from "react";
import HotelCard from "../../../components/hotelCard/hotelCard";
import { useFlightsStore } from "../../../store/flightsStore/flightsContext";

const SearchList = () => {
  const flightsStore = useFlightsStore();

  return (
    <Observer>
      {() => <div className="row">
         {

             flightsStore.flights.map((item,index)=>{
                 return(
                 <div className="col-md-6 col-12">
                     <HotelCard item={item} key={index}/>
                 </div>
             )})
         }
        </div>
      }
    </Observer>
  );
};

export default SearchList;
