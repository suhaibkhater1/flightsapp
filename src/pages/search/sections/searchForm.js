import React from "react";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from "@material-ui/pickers";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles(() => ({
  button: {
    margin: 25,
  },
}));

const SearchForm = ({ flightsStore }) => {
  const [fromDate, setFromDate] = React.useState(new Date());
  const [toDate, setToDate] = React.useState(new Date());
  const [error, setError] = React.useState(null);

  const search = () => {
    if (!toDate || !fromDate || new Date(toDate) < new Date(fromDate)) {
      setError("Dates Are Invalid");
      return;
    }
    flightsStore.setSearchParams({ dateRange:[fromDate,toDate]});
  };
  

  const classes = useStyles();

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <div className="container">
        <h1 className="pl-3 ">Search For Hotels</h1>
        <KeyboardDatePicker
          style={{ margin: "20px" }}
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-from"
          label="From"
          value={fromDate}
          onChange={setFromDate}
          KeyboardButtonProps={{
            "aria-label": "change date",
          }}
        />
        <KeyboardDatePicker
          style={{ margin: "20px" }}
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-to"
          label="To"
          value={toDate}
          onChange={setToDate}
          KeyboardButtonProps={{
            "aria-label": "change date",
          }}
        />
        <Button
          variant="outlined"
          color="primary"
          className={classes.button}
          onClick={search}
        >
          Search
        </Button>

        {error && <span style={{ color: "red" }}>{error}</span>}
      </div>
    </MuiPickersUtilsProvider>
  );
};

export default SearchForm;
