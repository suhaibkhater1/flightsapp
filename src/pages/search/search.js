import React from "react";
import { Observer } from "mobx-react";

import { useFlightsStore } from "../../store/flightsStore/flightsContext";
import SearchForm from "./sections/searchForm";
import "./search.css";
import { useHistory } from "react-router";

const Search = () => {
  const flightsStore = useFlightsStore();
  let history = useHistory()
  let redirect = false

  return (
    <Observer>
      {() => {
        if(flightsStore.loading && !redirect) redirect=true
        if(!flightsStore.loading && redirect)  history.push("/searchList")
        
        return (
          <>
            <div className="search">
              <div className="d-flex align-items-center justify-content-center overlay">
                <div className="search-box">
                  {flightsStore.loading && (
                    <div className="d-flex justify-content-center">Loading</div>
                  )}
                  {!flightsStore.loading && (
                    <SearchForm flightsStore={flightsStore} />
                  )}
                </div>
              </div>
            </div>
          </>
        );
      }}
    </Observer>
  );
};

export default Search;
