import React from 'react'
import { createFlightsStore } from './flightsStore'
import { useLocalObservable } from 'mobx-react'

const FlightsContext = React.createContext(null)

export const FlightsProvider = ({children}) => {
  const flightsStore = useLocalObservable(createFlightsStore)

  return <FlightsContext.Provider value={flightsStore}>
    {children}
  </FlightsContext.Provider>
}

export const useFlightsStore = () => React.useContext(FlightsContext)