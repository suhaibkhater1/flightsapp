import axios from "axios";

export function createFlightsStore() {
  return {
    searchParams: {},
    flights: [],
    setFlights(data){
      this.flights = data
    },
    setSearchParams(params) {
      this.searchParams={
        ...this.searchParams,
        ...params
      };
      this.getFlights();
    },
    async getFlights() {
      try {
        this.loading = true;
        const response = await axios.get(
          "https://www.mocky.io/v2/5eb8fcb12d0000d088357f2a"
        );
        let data = JSON.parse(
          response.data.replace(/\s\s+/g, "").replace(",]", "]")
        );
        this.setFlights(data);
        this.loading = false;
        const { priceRange, name } = this.searchParams;
        if (priceRange) this.searchByPriceRange(priceRange);
        if (name) this.searchByName(name);
      } catch (err) {
        this.loading = false;
      }
    },
    sortByName() {
      this.flights = this.flights.sort(function (firstItem, secondItem) {
        if (firstItem.name < secondItem.name) {
          return -1;
        }
        if (firstItem.name > secondItem.name) {
          return 1;
        }
        return 0;
      });
    },
    sortByPrice() {
      this.flights = this.flights.sort(function (firstItem, secondItem) {
        return firstItem.price - secondItem.price;
      });
    },
    searchByName(name) {
      this.flights = this.flights.filter((element) => {
        return element.name.toLowerCase().includes(name.toLowerCase());
      });
    },
    searchByPriceRange(price) {
      this.flights = this.flights.filter((element) => {
        return element.price >= price[0] && element.price <= price[1];
      });
    },
    loading: false,
  };
}
