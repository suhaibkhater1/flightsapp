import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Slider from "@material-ui/core/Slider";
import debounce from "lodash.debounce";

const useStyles = makeStyles({
  root: {
    width: 300,
    marginLeft: 20,
  },
});

function valuetext(value) {
  return `${value}`;
}

export default function RangeSlider({ title, onChange, min, max }) {
  const debouncedChange = React.useCallback(
    debounce((newValue) => onChange(newValue), 500),
    []
  );

  const classes = useStyles();
  const [value, setValue] = React.useState([min, max]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
    debouncedChange(newValue);
  };

  return (
    <div className={classes.root}>
      <Typography id="range-slider" gutterBottom>
        {title}
      </Typography>
      <Slider
        min={min}
        step={1}
        max={max}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        aria-labelledby="range-slider"
        getAriaValueText={valuetext}
      />
    </div>
  );
}
