import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import IconButton from "@material-ui/core/IconButton";
import SearchIcon from "@material-ui/icons/Search";

import "./searchField.css";

const SearchField = ({onChange}) => {
  const useStyles = makeStyles((theme) => ({
    input: {
      margin: 2,
    },
    iconButton: {
      padding: 10,
    },
  }));

  const classes = useStyles();
  return (
    <div className="search-container search-container d-flex justify-content-between align-items-center">
      <InputBase
        onChange={onChange}
        className={classes.input}
        placeholder="Hotel Name"
        inputProps={{ "aria-label": "Hotel Name" }}
      />
      <IconButton
        type="submit"
        className={classes.iconButton}
        aria-label="search"
      >
        <SearchIcon />
      </IconButton>
    </div>
  );
};
export default SearchField