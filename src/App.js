import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const SearchList = React.lazy(() => import("./pages/searchList/searchList"));
const Search = React.lazy(() => import("./pages/search/search"));
const renderLoader = () => (
  <div className="d-flex justify-content-center">Loading</div>
);

const App = () => {
  return (
    <Suspense fallback={renderLoader()}>
      <Router>
        <Switch>
          <Route path="/searchList">
            <SearchList />
          </Route>
          <Route path="/">
            <Search />
          </Route>
        </Switch>
      </Router>
    </Suspense>
  );
};

export default App;
