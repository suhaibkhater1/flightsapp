import React from 'react';
import renderer from 'react-test-renderer';

import SearchField from '../components/searchField/searchField';

it('renders correctly', () => {
  const tree = renderer.create(<SearchField/>).toJSON();
  expect(tree).toMatchSnapshot();
});
