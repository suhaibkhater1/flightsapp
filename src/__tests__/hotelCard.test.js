import React from 'react';
import renderer from 'react-test-renderer';

import HotelCard from '../components/hotelCard/hotelCard';

it('renders correctly', () => {
    const item = {
        name:"name",
        city:"city",
        price:"price",
        available_on:"available_on"
    }
  const tree = renderer.create(<HotelCard item={item} />).toJSON();
  expect(tree).toMatchSnapshot();
});
