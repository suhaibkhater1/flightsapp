import { createFlightsStore } from "../store/flightsStore/flightsStore";

describe("FlightsStore", () => {
  const mockData = [
    {
      name: "Kempinski Hotel Mall of the Emirates",
      price: "200",
      city: "dubai",
      available_on: "2020-10-21",
    },
    {
      name: "Address Dubai Mall",
      price: "250",
      city: "dubai",
      available_on: "2020-10-15",
    },
    {
      name: "Z Dubai Mall",
      price: "180",
      city: "dubai",
      available_on: "2020-10-15",
    },
  ];

  it("sets Flights", () => {
    const store = createFlightsStore();
    store.setFlights(mockData);
    expect(store.flights.length).toBe(3);
  });

  it("sort flights by name", () => {
    const store = createFlightsStore();
    store.setFlights(mockData);
    store.sortByName();

    expect(store.flights[0].name).toBe("Address Dubai Mall");
  });

  it("sort flights by price", () => {
    const store = createFlightsStore();
    store.setFlights(mockData);
    store.sortByPrice();

    expect(store.flights[0].price).toBe("180");
  });

  it("search by price range", () => {
    const store = createFlightsStore();
    store.setFlights(mockData);
    store.searchByPriceRange([180,200]);
   
    expect(store.flights.length).toBe(2);

    store.setFlights(mockData);
    store.searchByPriceRange([250,250]);
    expect(store.flights.length).toBe(1);
  });
});
