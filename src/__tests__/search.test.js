import React from "react";
import renderer from "react-test-renderer";

import Search from "../pages/search/search";
import { FlightsProvider } from "../store/flightsStore/flightsContext";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <FlightsProvider>
        <Search />
      </FlightsProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
