import React from "react";
import renderer from "react-test-renderer";

import SearchList from "../pages/searchList/searchList";
import { FlightsProvider } from "../store/flightsStore/flightsContext";

it("renders correctly", () => {
  const tree = renderer
    .create(
      <FlightsProvider>
        <SearchList />
      </FlightsProvider>
    )
    .toJSON();
  expect(tree).toMatchSnapshot();
});
