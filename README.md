

# Flights app
[![CircleCI](https://circleci.com/gh/circleci/circleci-docs/tree/master.svg?style=svg)](https://circleci.com/gh/circleci/circleci-docs/?branch=master)

# brief description
- An hotels search app with simple UI that uses ReactJS,Mobx, and MaterialUI 

# demo link
- loading might be slow as it is a free heroku server and starts when called only (cold start)
- [https://hotels-app-sra.herokuapp.com](https://hotels-app-sra.herokuapp.com/)


# Start the App
- clone the app
- run npm i && npm start
